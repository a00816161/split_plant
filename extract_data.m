clear all;
angulos = [ -30.67, -9.3299999, -29.33, -8, -28, -6.6700001, -26.67, -5.3299999, -25.33, -4, -24, -2.6700001, -22.67,-1.33, -21.33, 0, -20, 1.33, -18.67, 2.6700001, -17.33, 4, -16, 5.3299999,-14.67, 6.6700001, -13.33, 8, -12, 9.3299999, -10.67, 10.67 ]';
angulos = sort(angulos);
   
indices = [1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32]';
tabla = [1,17,2,18,3,19,4,20,5,21,6,22,7,23,8,24,9,25,10,26,11,27,12,28,13,29,14,30,15,31,16,32 ]';
values = [1:1:32]';
hash = containers.Map(indices,values);

way = csvread('data/runway1.csv',1,0);

n = size(way,1);

data = zeros(32,ceil(n/32));
data_indices = zeros(32,ceil(n/32));

laser = zeros(ceil(n/32),3,32);
laser(:,:,:) = 10000;

data(:,:) = 10000;
data_indices(:,:) = NaN;
i = 1;
j = 1;
id = hash( way(1,5)+1);
id_wt = way(1,5);

px_to_analize = [];
px_to_disc = [];
while (i<= n)
  temp = hash( way(i,5)+1);
  temp_wt = way(i,5);
   if temp_wt < id_wt
      j = j + 1;
   end
   id_wt = temp_wt;
   id = temp -1 ;
 %%  id = hash(id+1) -1;
   
  
  % if(way(i,7)> 10)
  %       i = i + 1;      
  %     continue;
       
  % end
 
   
 %%  if(way(i,3)>0)
 %%      i = i + 1;
 %%      continue;
 %%  end
   data(id + 1, j) = way(i,7);
   
   data_indices(id+1,j) = i;
   
   i = i + 1;      
end

%create the 2D dimensions

data_indices(data_indices == 0) = NaN;
data(data == 0) = 10000;

for ii=1:n
    temp = angulos( hash( way(ii,5)+1 ));
    %%Y
      way(ii,9) = way(ii,7)*cos(deg2rad(temp));
    %%Z
      way(ii,10) = way(ii,7)*sin(deg2rad(temp));

end