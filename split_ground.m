clear all;
extract_data;
ncols = size(data,2);

% params for line extraction
params.MIN_SEG_LENGTH = 0.1;
params.LINE_POINT_DIST_THRESHOLD = 0.05;
params.MIN_POINTS_PER_SEGMENT = 3;



      %% Create figures
f1 = figure(1);	% figure handle 'f1' 
set(f1, 'renderer', 'opengl') % we modify the figure's renderer 
ax = gca ; cla ; axis equal ; % axes handle 'ax'
axis([-20 10 -20 10 -1.5 5])
grid on

L_a = [];
points3d = line('parent', ax,...
    'marker', '*', ...
    'color', 'b', ...
    'LineStyle', 'none', ...
    'xdata',[], ...
    'ydata',[], ...
    'zdata',[]);
lines3d = [];
id_lines = 1;

for i=1:1
   idx = data_indices(~isnan(data_indices(:,i)),i);
  
   puntos = way(idx,9:10);
   
   [alpha, r, segend, seglen, pointIdx] =extractLines(puntos', params);
      num_lines = size(pointIdx,1);

   for j = 1:num_lines
      ini = pointIdx(j,1);
      fin = pointIdx(j,2);
      
      
      p_ini = way(idx(ini),9:10)';
      p_fin = way(idx(fin),9:10)';
      
      p_proy = p_fin;
      
      A = p_fin;
      B = p_ini;
      
      pr= dot(A,B);
      theta = acosd(pr);
      var_x = A(1) - B(1);
      var_y = A(2) - B(2);
      
     
      ang_l = atan2d(var_y,var_x);
         
      L_a = [L_a ang_l]; 
      
      if(ang_l>= -5 && ang_l <= 5)
            lines3d(id_lines) = line('xdata', [way(idx(ini),1), way(idx(fin),1) ], ...
                   'ydata', [way(idx(ini),2), way(idx(fin),2)], ...
                   'zdata', [way(idx(ini),3), way(idx(fin),3)], ...
                   'parent', ax, ...
                   'color', 'r');
      %         drawnow;
               
               id_lines = id_lines + 1;
      end
   end
end

% Update figure
   set(points3d,...
       'xdata', way(:,1), ...
       'ydata', way(:,2), ...
       'zdata', way(:,3));   
   drawnow;