function [theta, r, delta1, delta2] = eigen_line(x,y)

xi = sum(x);
yi = sum(y);
xi2 = sum(x.^2);
yi2 = sum(y.^2);

x_ = mean(x);
y_ = mean(y);

xy = sum(x.*y);

n = length(x);

X = xi2 - x_*xi;
Y = yi2 - y_*yi;
Z = xy - y_*xi;


sqt = sqrt(((X-Y)/2)^2 + Z^2);

delta1 = (X+Y)/2 + sqt;
delta2 = (X+Y)/2 - sqt;

V = [ Z/(delta1 - X) 1 ;
     Z/(delta2 - X) 1 ;
     1 Z/(delta1 - Y);
     1 Z/(delta2 -Y) ];
 
 theta = 0;
 
 if(delta1 < delta2)
     theta = atan2(Z,delta1-X);
 else
     theta = atan2(Z, delta2-Y);
 end
 

r = x_ * cos(theta)+ y_*sin(theta);