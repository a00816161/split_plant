nj = size(data,2);

scatter3(way(:,1),way(:,2),way(:,3),'.');
hold on

cl = ['y';'m';'c';'r';'g';'k';'y';'m';'c';'r';'g';'k';'y';'m';'c';'r';'g';'k'];
for jj=1:nj

ix= data_indices(:,jj);
ix = ix(~isnan(ix));
idx = way(ix,:);
n = size(idx,1);

%plot(idx(:,9),idx(:,10),'rx')

%labels = cellstr( num2str([1:n]') );
%text(idx(:,9), idx(:,10), labels, 'VerticalAlignment','bottom', ...
%                             'HorizontalAlignment','right')
%hold on

ini = struct( 'puntos', [], 'der',0,'izq',0, 'var_h',0.10, 'var_v',0.10, 'tipo', 1,...
    'alpha',0, 'r',0, 'segend', 0,'seglen',0, 'pointIdx',[]);

lista_vecinos = [];
for i=1:n
   %% circles(idx(i,2), idx(i,3),0.10);
   
   if( i == 1)
       %%% en radio
       el = ini;
       el.puntos = [i];
       
       if( euclidean( idx(i,9:10),idx(n,9:10)) <= 0.1) %% por la izquierda
           el.izq = n;
           el.puntos = [el.puntos n];
           el.tipo = 1;
       end
       
       if( euclidean( idx(i,9:10),idx(i+1,9:10)) <= 0.1) %% por la izquierda
           el.der = i+1;
           el.puntos = [el.puntos (i+1)];
           el.tipo = 1;
       end
       
       if(el.der ~=0 || el.izq ~=0)
          lista_vecinos = [lista_vecinos ; el]; 
          continue;
       end
       
       %%en horizontal
       if( abs(idx(i,3)-idx(n,3)) <= 0.75 && abs(idx(i,2)-idx(n,2))<= 0.75 ) %% en izquierda
            el.izq = n;
           el.puntos = [el.puntos n];
           el.tipo = 2;
       end
       
       if( abs(idx(i,3)-idx(i+1,3)) <= 0.75 && abs(idx(i,2)-idx(i+1,2))<= 0.75 ) %% en izquierda
            el.der = i+1;
           el.puntos = [el.puntos (i+1)];
           el.tipo = 2;
       end
       
        if(el.der ~=0 || el.izq ~=0)
          lista_vecinos = [lista_vecinos ; el]; 
          continue;
       end
       
       %%en vertical
       
        if( abs(idx(i,2)-idx(n,2)) <= 0.75 && abs(idx(i,3)-idx(n,3))<= 0.75 ) %% en izquierda
            el.izq = n;
           el.puntos = [el.puntos n];
           el.tipo = 3;
       end
       
       if( abs(idx(i,2)-idx(i+1,2)) <= 0.75 && abs(idx(i,3)-idx(i+1,3))<= 0.75 ) %% en izquierda
            el.der = i+1;
           el.puntos = [el.puntos (i+1)];
           el.tipo = 3;
       end
       
       
   elseif (i ==n)
       %%% en radio
       el = ini;
       el.puntos = [i];
       
       if( euclidean( idx(i,9:10),idx(i-1,9:10)) <= 0.1) %% por la izquierda
           el.izq = i-1;
           el.puntos = [el.puntos (i-1)];
           el.tipo = 1;
       end
       
       if( euclidean( idx(i,9:10),idx(1,9:10)) <= 0.1) %% por la derecha
           el.der = 1;
           el.puntos = [el.puntos (1)];
           el.tipo =1;
       end
       
       if(el.der ~=0 || el.izq ~=0)
          lista_vecinos = [lista_vecinos ; el]; 
          continue;
       end
       
       %%en horizontal
       if( abs(idx(i,3)-idx(i-1,3)) <= 0.1 && abs(idx(i,2)-idx(i-1,2))<= 0.75 ) %% en izquierda
            el.izq = i-1;
           el.puntos = [el.puntos (i-1)];
           el.tipo = 2;
       end
       
       if( abs(idx(i,3)-idx(1,3)) <= 0.1 && abs(idx(i,2)-idx(1,2))<= 0.75 ) %% en derecha
            el.der = 1;
           el.puntos = [el.puntos (1)];
           el.tipo = 2;
       end
       
        if(el.der ~=0 || el.izq ~=0)
          lista_vecinos = [lista_vecinos ; el]; 
          continue;
       end
       
       %%en vertical
       
        if( abs(idx(i,2)-idx(i-1,2)) <= 0.1 && abs(idx(i,3)-idx(i-1,3))<= 0.75 ) %% en izquierda
            el.izq = i-1;
           el.puntos = [el.puntos (i-1)];
           el.tipo = 3;
       end
       
       if( abs(idx(i,2)-idx(1,2)) <= 0.1 && abs(idx(i,3)-idx(1,3))<= 0.75 ) %% en derecha
            el.der = 1;
           el.puntos = [el.puntos (1)];
           el.tipo = 3;
       end
       
       lista_vecinos = [lista_vecinos ; el];

   else
         %%% en radio
       el = ini;
       el.puntos = [i];
       
       if( euclidean( idx(i,9:10),idx(i-1,9:10)) <= 0.1) %% por la izquierda
           el.izq = i-1;
           el.puntos = [el.puntos (i-1)];
           el.tipo = 1;
       end
       
       if( euclidean( idx(i,9:10),idx(i+1,9:10)) <= 0.1) %% por la derecha
           el.der = i+1;
           el.puntos = [el.puntos (i+1)];
           el.tipo = 1;
       end
       
       if(el.der ~=0 || el.izq ~=0)
          lista_vecinos = [lista_vecinos ; el]; 
          continue;
       end
       
       %%en horizontal
       if( abs(idx(i,3)-idx(i-1,3)) <= 0.1 && abs(idx(i,2)-idx(i-1,2))<= 0.75 ) %% en izquierda
            el.izq = i-1;
           el.puntos = [el.puntos (i-1)];
           el.tipo = 2;
       end
       
       if( abs(idx(i,3)-idx(i+1,3)) <= 0.1 && abs(idx(i,2)-idx(i+1,2))<= 0.75 ) %% en derecha
            el.der = i+1;
           el.puntos = [el.puntos (i+1)];
           el.tipo = 2;
       end
       
        if(el.der ~=0 || el.izq ~=0)
          lista_vecinos = [lista_vecinos ; el]; 
          continue;
       end
       
       %%en vertical
       
        if( abs(idx(i,2)-idx(i-1,2)) <= 0.1 && abs(idx(i,3)-idx(i-1,3))<= 0.75 ) %% en izquierda
            el.izq = i-1;
           el.puntos = [el.puntos (i-1)];
           el.tipo = 3;
       end
       
       if( abs(idx(i,2)-idx(i+1,2)) <= 0.1 && abs(idx(i,3)-idx(i+1,3))<= 0.75 ) %% en derecha
            el.der = i+1;
           el.puntos = [el.puntos (i+1)];
           el.tipo = 3;
       end
      lista_vecinos = [lista_vecinos ; el]; 

   end

end

flag = false;

clusters = [];
inic = struct('puntos',[]);
temp = inic;
i = 1;
n = size(lista_vecinos,1);
while i<=n
    if(flag ==false)
       temp = inic;
       temp.puntos = [temp.puntos i];
       if i~= n
        if(isempty(find(lista_vecinos(i+1).puntos==i)) && (lista_vecinos(i).der == 0)) %% si no aparece 
          clusters = [clusters temp];
          flag = false;
        else
          flag = true;
        end
       else
          % if(isempty(find(lista_vecinos(1).puntos==i)))
          %     temp.puntos = [temp.puntos 1];
          %     flag = false;
          % end
           clusters = [clusters temp];
  
       end
    else
        temp.puntos = [temp.puntos i];
        
        if i ~= n
            if(isempty(find(lista_vecinos(i+1).puntos==i)) && (lista_vecinos(i).der == 0)) %% si no aparece 
                clusters = [clusters temp];
                flag = false;
            else
               flag = true;
            end
       else
           %if(isempty(find(lista_vecinos(1).puntos==i)))
           %    temp.puntos = [temp.puntos 1];
           %    flag = false;
           %end
           clusters = [clusters temp];
  
       end
        
    end
    i = i + 1;
end

nm = size(clusters,2);
clu = [];

iu=1;
for ii=1:nm
    if( size(clusters(ii).puntos,2) > 1)
       clu = [clu clusters(ii)]; 
       scatter3(idx(clusters(ii).puntos,1),idx(clusters(ii).puntos,2),idx(clusters(ii).puntos,3),cl(iu));
       iu = iu + 1;
    end
end

end